import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";

class Dashboard extends Component {
  render() {
    return (
      <>
        <Header />
        <section>{this.props.children}</section>
        <Footer />
      </>
    );
  }
}

export default withRouter(Dashboard);
