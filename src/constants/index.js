import Home from "../containers/home";

export const ROUTES = [
  {
    path: "/",
    name: "Home",
    exact: true,
    component: Home,
  },
];
