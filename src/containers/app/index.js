import React, { useMemo } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import DefaultLayoutRoute from "../../components/DefaultLayoutRoute";
import { ROUTES } from "../../constants";
import NotFoundPage from "../notFoundPage";

function App() {
  const renderDefaultRoutes = useMemo(() => {
    let xhtml = null;
    xhtml = ROUTES.map((item, index) => {
      return (
        <DefaultLayoutRoute
          component={item.component}
          routes={item.routes}
          exact={item.exact}
          key={index}
          path={item.path}
          meta={item}
        />
      );
    });
    return xhtml;
  }, []);

  const renderRoutes = useMemo(() => {
    let xhtml = null;
    xhtml = (
      <Switch>
        {renderDefaultRoutes()}
        <Route exact path="/404" component={NotFoundPage} />
        <Redirect to="/" />
      </Switch>
    );
    return xhtml;
  }, [renderDefaultRoutes]);

  return (
    <div className="App">
      <Router>{renderRoutes()}</Router>
    </div>
  );
}

export default App;
