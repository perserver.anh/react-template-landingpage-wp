import React from "react";
import { withRouter } from "react-router-dom";

const Home = () => {
  return <h1>Home</h1>;
};

export default withRouter(Home);
