import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class NotFoundPage extends Component {
  render() {
    return <h1>404</h1>;
  }
}

export default withRouter(NotFoundPage);
